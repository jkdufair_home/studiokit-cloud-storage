﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using StudioKit.Cloud.Storage.Blob;
using StudioKit.Cloud.Storage.Extensions;
using StudioKit.Cloud.Storage.Queue;
using StudioKit.Encryption;
using System;
using System.Linq;
using System.Reflection;

namespace StudioKit.Cloud.Storage
{
	public class AzureStorage
	{
		private static CloudStorageAccount _storageAccount;

		/// <summary>
		/// Shared CloudStorageAccount instance for easy access.
		/// Uses StorageConnectionString.
		/// </summary>
		/// <see cref="StorageConnectionString" />
		public static CloudStorageAccount StorageAccount
			=> _storageAccount ?? (_storageAccount = CloudStorageAccount.Parse(StorageConnectionString));

		/// <summary>
		/// Connection string for Azure Storage Account using CloudConfiguration for "StorageConnectionString"
		/// </summary>
		public static string StorageConnectionString => EncryptedConfigurationManager.GetSetting(StorageAppSetting.StorageConnectionString);

		/// <summary>
		/// Configure all required Azure Storage items.
		/// Creates Blob Containers, Tables, and Queues if they do not exist. Searches assembly for subclasses of
		/// AzureBlobStorage,
		/// and implementations of IQueueManager and IAzureTableRepository.
		/// </summary>
		public static void Configure()
		{
			// Create Blob Containers
			var blobClient = StorageAccount.CreateCloudBlobClient();
			var assemblies = AppDomain.CurrentDomain.GetAssemblies();
			var assemblyTypes = assemblies
				.SelectMany(assembly =>
				{
					try
					{
						var types = assembly.GetTypes();
						return types;
					}
					catch (ReflectionTypeLoadException e)
					{
						return e.Types.Where(t => t != null);
					}
				}).ToList();
			var blobStorageTypes = assemblyTypes
				.Where(type => type.IsSubclassOf(typeof(AzureBlobStorage)));
			foreach (var type in blobStorageTypes)
			{
				var containerName = type.ToBlobContainerName();
				var accessType = (BlobAccessType)type.GetProperty("AccessType", BindingFlags.Public | BindingFlags.Static)
					.GetValue(null);

				var container = blobClient.GetContainerReference(containerName);
				container.CreateIfNotExists();

				if (!accessType.Equals(BlobAccessType.Public)) continue;
				var permissions = new BlobContainerPermissions
				{
					PublicAccess = BlobContainerPublicAccessType.Blob
				};
				container.SetPermissions(permissions);
			}

			// Create Queue Manager
			var queueClient = StorageAccount.CreateCloudQueueClient();
			var queueTypes = assemblyTypes
				.Where(type => type.IsGenericTypeOf(typeof(IQueueManager<>)) && !type.IsGenericType && !type.IsInterface);
			foreach (var queue in queueTypes.Select(type => type.ToQueueName()).Select(queueClient.GetQueueReference))
				queue.CreateIfNotExists();
		}
	}
}