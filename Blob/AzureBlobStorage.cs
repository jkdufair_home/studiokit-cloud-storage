using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using StudioKit.Cloud.Storage.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Cloud.Storage.Blob
{
	public class AzureBlobStorage : IBlobStorage
	{
		protected readonly CloudBlobContainer BlobContainer;

		public AzureBlobStorage(string containerName)
		{
			var client = AzureStorage.StorageAccount.CreateCloudBlobClient();
			BlobContainer = client.GetContainerReference(containerName ?? GetType().ToBlobContainerName());
		}

		public BlobProperties BlockBlobProperties(string blobName)
		{
			var blockBlob = BlobContainer.GetBlockBlobReference(blobName);
			blockBlob.FetchAttributes();
			return blockBlob.Properties;
		}

		#region IBlobStorage Statics

		public static BlobAccessType AccessType => throw new NotImplementedException();

		public static string BlobEndPointString => AzureStorage.StorageAccount.BlobEndpoint.ToString();

		public static Uri UriForBlockBlob(string containerName, string blobName)
		{
			return new Uri(CombinePath(BlobEndPointString, containerName, blobName));
		}

		/// <summary>
		/// Combine two paths, emulating System.IO.File.CombinePath
		/// </summary>
		/// <param name="path1">The first path</param>
		/// <param name="path2">The second path</param>
		/// <returns></returns>
		public static string CombinePath(string path1, string path2)
		{
			if (string.IsNullOrWhiteSpace(path1)) return path2;
			return string.IsNullOrWhiteSpace(path2)
				? path1
				: $"{path1}{(path1.EndsWith("/") ? "" : "/")}{path2}";
		}

		/// <summary>
		/// Combine three paths, emulating System.IO.File.CombinePath
		/// </summary>
		/// <param name="path1">The first path</param>
		/// <param name="path2">The second path</param>
		/// <param name="path3">The third path</param>
		/// <returns></returns>
		public static string CombinePath(string path1, string path2, string path3)
		{
			var path = CombinePath(path1, path2);
			return CombinePath(path, path3);
		}

		#endregion IBlobStorage Statics

		#region IBlobStorage Implementation

		public Uri ContainerUri
		{
			get
			{
				var result = CombinePath(BlobEndPointString, BlobContainer.Name);
				return new Uri(result);
			}
		}

		/// <summary>
		/// Answer the URI for a block blob at a given path
		/// </summary>
		/// <param name="blobName">The block blob in question</param>
		/// <returns>A URI for the blob in the application's container</returns>
		public Uri UriForBlockBlob(string blobName)
		{
			return UriForBlockBlob(BlobContainer.Name, blobName);
		}

		/// <summary>
		/// Exists for compatibility with legacy code.  Paths can always be written in Azure storage
		/// </summary>
		/// <param name="path">The path to the resource</param>
		/// <returns>True</returns>
		public bool PathExists(string path)
		{
			return true;
		}

		/// <summary>
		/// Exists for compatibility with legacy code.  Paths are an abstraction in Azure storage and, as such
		/// always "exist"
		/// </summary>
		/// <param name="path">The path whose existence must be ensured</param>
		public void EnsurePathExists(string path) { }

		/// <inheritdoc />
		/// <summary>
		/// Answer whether a block blob exists
		/// </summary>
		/// <param name="blobName">The blob name</param>
		/// <returns>Whether or not the requested blob exists</returns>
		public bool BlockBlobExists(string blobName)
		{
			var blockBlob = BlobContainer.GetBlockBlobReference(blobName);
			return blockBlob.Exists();
		}

		/// <inheritdoc />
		/// <summary>
		/// Write a block blob to a stream
		/// </summary>
		/// <param name="stream">The stream to which the block blob should be written</param>
		/// <param name="blobName">The blob name</param>
		public void WriteBlockBlobToStream(Stream stream, string blobName)
		{
			var blockBlob = BlobContainer.GetBlockBlobReference(blobName);
			blockBlob.DownloadToStream(stream);
		}

		/// <inheritdoc />
		/// <summary>
		/// Write a block blob to a stream
		/// </summary>
		/// <param name="stream">The stream to which the block blob should be written</param>
		/// <param name="pathToBlob">The blob name</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		public async Task WriteBlockBlobToStreamAsync(Stream stream, string pathToBlob, CancellationToken cancellationToken = default(CancellationToken))
		{
			var blockBlob = BlobContainer.GetBlockBlobReference(pathToBlob);
			await blockBlob.DownloadToStreamAsync(stream, cancellationToken);
		}

		/// <summary>
		/// Upload a stream to Azure storage as a block blob
		/// </summary>
		/// <param name="stream">The stream from which to write</param>
		/// <param name="pathToBlob">The path at which the block blob should be created, including the filename</param>
		/// <param name="contentType">The MIME type of the blob to be created</param>
		public void UploadBlockBlob(Stream stream, string pathToBlob, string contentType)
		{
			var blockBlob = BlobContainer.GetBlockBlobReference(pathToBlob);
			blockBlob.UploadFromStream(stream);
			blockBlob.Properties.ContentType = contentType;
			blockBlob.SetProperties();
		}

		/// <inheritdoc />
		/// <summary>
		/// Upload a stream to Azure storage as a block blob
		/// </summary>
		/// <param name="stream">The stream from which to write</param>
		/// <param name="pathToBlob">The path at which the block blob should be created, including the filename</param>
		/// <param name="contentType">The MIME type of the blob to be created</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		public async Task UploadBlockBlobAsync(Stream stream, string pathToBlob, string contentType, CancellationToken cancellationToken = default(CancellationToken))
		{
			var blockBlob = BlobContainer.GetBlockBlobReference(pathToBlob);
			await blockBlob.UploadFromStreamAsync(stream, cancellationToken);
			blockBlob.Properties.ContentType = contentType;
			await blockBlob.SetPropertiesAsync(cancellationToken);
		}

		/// <summary>
		/// Answer the contents of a block blob as a byte array
		/// </summary>
		/// <param name="blobName">The blob name</param>
		/// <returns>A byte array with the contents of the block blob</returns>
		public byte[] ReadAllBytes(string blobName)
		{
			using (var stream = new MemoryStream())
			{
				WriteBlockBlobToStream(stream, blobName);
				stream.Position = 0;
				return stream.ToArray();
			}
		}

		/// <inheritdoc />
		/// <summary>
		/// Delete the block blob at the given path
		/// </summary>
		/// <param name="blobName">The block blob name to be deleted</param>
		public void DeleteBlockBlob(string blobName)
		{
			var blockBlob = BlobContainer.GetBlockBlobReference(blobName);
			blockBlob.Delete();
		}

		/// <inheritdoc />
		/// <summary>
		/// Delete the block blob at the given path
		/// </summary>
		/// <param name="blobName">The block blob name to be deleted</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		public async Task DeleteBlockBlobAsync(string blobName, CancellationToken cancellationToken = default(CancellationToken))
		{
			var blockBlob = BlobContainer.GetBlockBlobReference(blobName);
			await blockBlob.DeleteAsync(cancellationToken);
		}

		/// <inheritdoc />
		/// <summary>
		/// Copy a block blob to another path.  This method may return before the copy is complete.  There is
		/// not currently a mechanism for a callback here in StudioKit.Cloud.  You should write one.
		/// </summary>
		/// <param name="pathToSourceBlob">The path to the source block blob</param>
		/// <param name="pathToDestinationBlob">The path to the destination block blob</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		public async Task CopyBlockBlobAsync(string pathToSourceBlob, string pathToDestinationBlob,
			CancellationToken cancellationToken = default(CancellationToken))
		{
			var sourceBlob = BlobContainer.GetBlockBlobReference(pathToSourceBlob);
			var destinationBlob = BlobContainer.GetBlockBlobReference(pathToDestinationBlob);
			await destinationBlob.StartCopyAsync(sourceBlob, cancellationToken);
		}

		/// <inheritdoc />
		/// <summary>
		/// Copy a block blob and wait up to 10 seconds (default) for copy to complete
		/// </summary>
		/// <param name="pathToSourceBlob">The path to the source block blob</param>
		/// <param name="pathToDestinationBlob">The path to the destination block blob</param>
		/// <param name="timeout">The amount of time you are willing to wait for the blob to copy</param>
		public void CopyBlockBlob(string pathToSourceBlob, string pathToDestinationBlob, int timeout = 10000)
		{
			var sourceBlob = BlobContainer.GetBlockBlobReference(pathToSourceBlob);
			var destinationBlob = BlobContainer.GetBlockBlobReference(pathToDestinationBlob);
			var task = Task.Factory.FromAsync(destinationBlob.BeginStartCopy(sourceBlob, null, null),
				destinationBlob.EndStartCopy);
			task.Wait(timeout);
		}

		/// <inheritdoc />
		/// <summary>
		/// Given a path (where, in Azure, paths are just part of the blob name), answer the block blobs at that path.
		/// (i.e. all block blobs whose names start with that same "path"
		/// </summary>
		/// <param name="path">The path at which the blobs may exist</param>
		/// <returns>An ordered collection of URLs as strings</returns>
		public IEnumerable<string> BlockBlobsAtPath(string path)
		{
			var blockBlobs = BlobContainer.ListBlobs(path + "/");
			return blockBlobs.Where(bb => bb is CloudBlockBlob).Select(bb => bb.Uri.ToString());
		}

		/// <inheritdoc />
		/// <summary>
		/// Given a path (where, in Azure, paths are just part of the blob name), answer the block blobs at that path.
		/// (i.e. all block blobs whose names start with that same "path" and prefix string
		/// </summary>
		/// <param name="path"></param>
		/// <param name="prefix"></param>
		/// <returns></returns>
		public IEnumerable<string> BlockBlobsAtPathWithPrefix(string path, string prefix)
		{
			try
			{
				var blockBlobs = BlobContainer.ListBlobs(CombinePath(path, prefix));
				return blockBlobs.Where(bb => bb is CloudBlockBlob).Select(bb => ((CloudBlockBlob)bb).Name).ToList();
			}
			catch (StorageException)
			{
				return new List<string>();
			}
		}

		#endregion IBlobStorage Implementation
	}
}