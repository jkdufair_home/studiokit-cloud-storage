﻿namespace StudioKit.Cloud.Storage.Blob
{
	public enum BlobAccessType
	{
		Private,
		Public
	}
}