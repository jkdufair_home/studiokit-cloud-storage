using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Cloud.Storage.Blob
{
	public interface IBlobStorage
	{
		/// <summary>
		/// Return the base Uri for the BlobContainer
		/// </summary>
		Uri ContainerUri { get; }

		/// <summary>
		/// Exists for compatibility with legacy code.  Paths can always be written in Azure storage
		/// </summary>
		/// <param name="path">The path to the resource</param>
		/// <returns>True</returns>
		bool PathExists(string path);

		/// <summary>
		/// Exists for compatibility with legacy code.  Paths are an abstraction in Azure storage and, as such
		/// always "exist"
		/// </summary>
		/// <param name="path">The path whose existence must be ensured</param>
		void EnsurePathExists(string path);

		/// <summary>
		/// Answer whether a block blob exists
		/// </summary>
		/// <param name="pathToBlockBlob">The path to the block blob</param>
		/// <returns>Whether or not the requested blob, including its path, exists</returns>
		bool BlockBlobExists(string pathToBlockBlob);

		/// <summary>
		/// Write a block blob to a stream
		/// </summary>
		/// <param name="stream">The stream to which the block blob should be written</param>
		/// <param name="pathToBlob">The path to the block blob</param>
		void WriteBlockBlobToStream(Stream stream, string pathToBlob);

		/// <summary>
		/// Write a block blob to a stream asynchronously
		/// </summary>
		/// <param name="stream">The stream to which the block blob should be written</param>
		/// <param name="pathToBlob">The path to the block blob</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		Task WriteBlockBlobToStreamAsync(Stream stream, string pathToBlob, CancellationToken cancellationToken = default(CancellationToken));

		/// <summary>
		/// Upload a stream to Azure storage as a block blob
		/// </summary>
		/// <param name="stream">The stream from which to write</param>
		/// <param name="pathToBlob">The path at which the block blob should be created, including the filename</param>
		/// <param name="contentType">The MIME type of the blob to be created</param>
		void UploadBlockBlob(Stream stream, string pathToBlob, string contentType);

		/// <summary>
		/// Upload a stream to Azure storage as a block blob
		/// </summary>
		/// <param name="stream">The stream from which to write</param>
		/// <param name="pathToBlob">The path at which the block blob should be created, including the filename</param>
		/// <param name="contentType">The MIME type of the blob to be created</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		Task UploadBlockBlobAsync(Stream stream, string pathToBlob, string contentType, CancellationToken cancellationToken = default(CancellationToken));

		/// <summary>
		/// Answer the contents of a block blob as a byte array
		/// </summary>
		/// <param name="pathToBlob">The path to the block blob</param>
		/// <returns>A byte array with the contents of the block blob</returns>
		byte[] ReadAllBytes(string pathToBlob);

		/// <summary>
		/// Delete the block blob at the given path
		/// </summary>
		/// <param name="pathToBlob">The block blob to be deleted</param>
		void DeleteBlockBlob(string pathToBlob);

		/// <summary>
		/// Delete the block blob at the given path
		/// </summary>
		/// <param name="pathToBlob">The block blob to be deleted</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		Task DeleteBlockBlobAsync(string pathToBlob, CancellationToken cancellationToken = default(CancellationToken));

		/// <summary>
		/// Copy a block blob to another path.  This method may return before the copy is complete.  There is
		/// not currently a mechanism for a callback here in StudioKit.Cloud.  You should write one.
		/// </summary>
		/// <param name="pathToSourceBlob">The path to the source block blob</param>
		/// <param name="pathToDestinationBlob">The path to the destination block blob</param>
		/// <param name="cancellationToken">A <see cref="CancellationToken"/> from the caller</param>
		Task CopyBlockBlobAsync(string pathToSourceBlob, string pathToDestinationBlob,
			CancellationToken cancellationToken);

		/// <summary>
		/// Copy a block blob and wait up to 10 seconds (default) for copy to complete
		/// </summary>
		/// <param name="pathToSourceBlob">The path to the source block blob</param>
		/// <param name="pathToDestinationBlob">The path to the destination block blob</param>
		/// <param name="timeout">The amount of time you are willing to wait for the blob to copy</param>
		void CopyBlockBlob(string pathToSourceBlob, string pathToDestinationBlob, int timeout = 10000);

		/// <summary>
		/// Given a path (where, in Azure, paths are just part of the blob name), answer the block blobs at that path.
		/// (i.e. all block blobs whose names start with that same "path"
		/// </summary>
		/// <param name="path">The path at which the blobs may exist</param>
		/// <returns>An ordered collection of URLs as strings</returns>
		IEnumerable<string> BlockBlobsAtPath(string path);

		/// <summary>
		/// Given a path (where, in Azure, paths are just part of the blob name), answer the block blobs at that path.
		/// (i.e. all block blobs whose names start with that same "path" and prefix string
		/// </summary>
		/// <param name="path"></param>
		/// <param name="prefix"></param>
		/// <returns></returns>
		IEnumerable<string> BlockBlobsAtPathWithPrefix(string path, string prefix);

		/// <summary>
		/// Answer the URI for a block blob at a given path
		/// </summary>
		/// <param name="pathToBlockBlob">The block blob in question</param>
		/// <returns>A URI for the blob in the application's container</returns>
		Uri UriForBlockBlob(string pathToBlockBlob);
	}
}