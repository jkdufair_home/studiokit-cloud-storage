﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace StudioKit.Cloud.Storage.Extensions
{
	public static class TypeExtensions
	{
		/// <summary>
		/// Convert a Type Name into a Table Name to use in Azure Storage Table
		/// e.g. TopicDocumentRepository -> TopicDocument
		/// </summary>
		/// <param name="t">The Type</param>
		/// <returns>The table name</returns>
		public static string ToTableName(this Type t)
		{
			return t.Name.Substring(0, t.Name.IndexOf("Repository", StringComparison.Ordinal));
		}

		/// <summary>
		/// Convert a Type Name into a Blob Container Name to us in Azure Storage Blob Container
		/// e.g. TopicDocumentStorage -> topic-document
		/// </summary>
		/// <param name="t">The Type</param>
		/// <returns>The blob container name</returns>
		public static string ToBlobContainerName(this Type t)
		{
			var name = t.Name.Substring(0, t.Name.IndexOf("Storage", StringComparison.Ordinal));
			name = Regex.Replace(name, "([A-Z])([A-Z][a-z])|([a-z0-9])([A-Z])", "$1$3-$2$4").ToLowerInvariant();
			return name;
		}

		/// <summary>
		/// Convert a Type Name into a Queue Name to use in Azure Storage Queues
		/// </summary>
		/// <param name="t">The Type</param>
		/// <returns>The queue name</returns>
		public static string ToQueueName(this Type t)
		{
			var name = t.Name.Substring(0, t.Name.IndexOf("QueueManager", StringComparison.Ordinal));
			name = Regex.Replace(name, "([A-Z])([A-Z][a-z])|([a-z0-9])([A-Z])", "$1$3-$2$4").ToLowerInvariant();
			return name;
		}

		public static bool IsGenericTypeOf(this Type t, Type genericDefinition)
		{
			Type[] parameters;
			return IsGenericTypeOf(t, genericDefinition, out parameters);
		}

		public static bool IsGenericTypeOf(this Type t, Type genericDefinition, out Type[] genericParameters)
		{
			genericParameters = new Type[] { };
			if (!genericDefinition.IsGenericType)
			{
				return false;
			}

			var isMatch = t.IsGenericType && t.GetGenericTypeDefinition() == genericDefinition.GetGenericTypeDefinition();
			if (!isMatch && t.BaseType != null)
			{
				isMatch = IsGenericTypeOf(t.BaseType, genericDefinition, out genericParameters);
			}
			if (!isMatch && genericDefinition.IsInterface && t.GetInterfaces().Any())
			{
				foreach (var i in t.GetInterfaces())
				{
					if (i.IsGenericTypeOf(genericDefinition, out genericParameters))
					{
						isMatch = true;
						break;
					}
				}
			}

			if (isMatch && !genericParameters.Any())
			{
				genericParameters = t.GetGenericArguments();
			}
			return isMatch;
		}
	}
}