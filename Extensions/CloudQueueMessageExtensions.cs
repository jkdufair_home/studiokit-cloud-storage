﻿using Microsoft.WindowsAzure.Scheduler.Models;
using Microsoft.WindowsAzure.Storage.Queue;
using StudioKit.Diagnostics;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Xml.Serialization;

namespace StudioKit.Cloud.Storage.Extensions
{
	public static class CloudQueueMessageExtensions
	{
		private static T DeserializeXml<T>(string value, ILogger logger, IErrorHandler errorHandler, bool shouldLogException = false)
		{
			if (string.IsNullOrWhiteSpace(value))
				return default(T);
			T obj;
			using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(value)))
			{
				var serializer = new XmlSerializer(typeof(T));
				try
				{
					obj = (T)serializer.Deserialize(ms);
				}
				catch (Exception e)
				{
					ms.Position = 0;
					obj = default(T);
					if (!shouldLogException)
						return obj;
					var message = $"Deserialize to type {typeof(T)} failed with value \"{value}\"";
					logger.Error(message);
					errorHandler.CaptureException(new Exception(message, e));
				}
			}
			return obj;
		}

		private static T DeserializeJson<T>(string value, ILogger logger, IErrorHandler errorHandler, bool shouldLogException = false)
		{
			if (string.IsNullOrWhiteSpace(value))
				return default(T);
			T obj;
			using (var ms = new MemoryStream(Encoding.Unicode.GetBytes(value)))
			{
				var serializer = new DataContractJsonSerializer(typeof(T));
				try
				{
					obj = (T)serializer.ReadObject(ms);
				}
				catch (Exception e)
				{
					ms.Position = 0;
					obj = default(T);
					if (!shouldLogException)
						return obj;
					var message = $"Deserialize to type {typeof(T)} failed with value \"{value}\"";
					logger.Error(message);
					errorHandler.CaptureException(new Exception(message, e));
				}
			}
			return obj;
		}

		public static T Deserialize<T>(this CloudQueueMessage message, ILogger logger, IErrorHandler errorHandler)
		{
			var value = message.AsString;
			// Azure Scheduler Job Collection nests the final message inside a StorageQueueMessage as XML
			var storageQueueMessage =
				DeserializeXml<StorageQueueMessage>(value, logger, errorHandler);
			if (storageQueueMessage != null)
				value = storageQueueMessage.Message;
			return DeserializeJson<T>(value, logger, errorHandler, true);
		}
	}
}