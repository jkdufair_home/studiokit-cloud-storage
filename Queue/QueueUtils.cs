﻿using Microsoft.WindowsAzure.Storage.Queue;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Text;

namespace StudioKit.Cloud.Storage.Queue
{
	public static class QueueUtils
	{
		public static string Serialize<TQueueMessage>(object obj) where TQueueMessage : AzureQueueMessage
		{
			var serializer = new DataContractJsonSerializer(typeof(TQueueMessage));
			using (var ms = new MemoryStream())
			{
				serializer.WriteObject(ms, obj);
				return Encoding.Default.GetString(ms.ToArray());
			}
		}

		public static CloudQueueMessage CloudQueueMessage<TQueueMessage>(TQueueMessage queueMessage) where TQueueMessage : AzureQueueMessage
		{
			return new CloudQueueMessage(Serialize<TQueueMessage>(queueMessage));
		}
	}
}