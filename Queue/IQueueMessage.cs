﻿using Microsoft.WindowsAzure.Storage.Queue;

namespace StudioKit.Cloud.Storage.Queue
{
	public interface IQueueMessage
	{
		CloudQueueMessage CloudQueueMessage { get; set; }
	}
}