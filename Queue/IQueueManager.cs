﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Cloud.Storage.Queue
{
	/// <summary>
	/// Interface for interacting with a queue of specific message type
	/// </summary>
	/// <typeparam name="T">The type of queue message</typeparam>
	public interface IQueueManager<T> where T : IQueueMessage
	{
		void AddMessage(T message, TimeSpan? timeToLive = null, TimeSpan? initialVisibilityDelay = null,
			QueueRequestOptions options = null, OperationContext operationContext = null);

		Task AddMessageAsync(T message, TimeSpan? timeToLive = null, TimeSpan? initialVisibilityDelay = null,
			QueueRequestOptions options = null,
			OperationContext operationContext = null,
			CancellationToken cancellationToken = default(CancellationToken));

		void DeleteMessage(T message);

		Task DeleteMessageAsync(T message, CancellationToken cancellationToken = default(CancellationToken));

		T GetMessage(TimeSpan? visibilityTimeout = null, QueueRequestOptions options = null,
			OperationContext operationContext = null);

		Task<T> GetMessageAsync(TimeSpan? visibilityTimeout = null, QueueRequestOptions options = null,
			OperationContext operationContext = null, CancellationToken cancellationToken = default(CancellationToken));

		T[] GetMultipleMessages(int maxMessages, TimeSpan? visibilityTimeout = null, QueueRequestOptions options = null,
			OperationContext operationContext = null);

		Task<T[]> GetMultipleMessagesAsync(int maxMessages, TimeSpan? visibilityTimeout = null,
			QueueRequestOptions options = null,
			OperationContext operationContext = null,
			CancellationToken cancellationToken = default(CancellationToken));

		void UpdateMessage(T message, TimeSpan visibilityTimeout, MessageUpdateFields updateFields);

		Task UpdateMessageAsync(T message, TimeSpan visibilityTimeout, MessageUpdateFields updateFields, CancellationToken cancellationToken = default(CancellationToken));

		void Clear();

		Task ClearAsync(CancellationToken cancellationToken = default(CancellationToken));
	}
}