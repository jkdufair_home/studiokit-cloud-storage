﻿using Microsoft.WindowsAzure.Storage.Queue;
using System.Runtime.Serialization;

namespace StudioKit.Cloud.Storage.Queue
{
	[DataContract]
	public class AzureQueueMessage : IQueueMessage
	{
		[IgnoreDataMember]
		public CloudQueueMessage CloudQueueMessage { get; set; }
	}
}