﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Queue;
using StudioKit.Cloud.Storage.Extensions;
using StudioKit.Diagnostics;
using StudioKit.ErrorHandling.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace StudioKit.Cloud.Storage.Queue
{
	public class AzureQueueManager<TQueueMessage> : IQueueManager<TQueueMessage> where TQueueMessage : AzureQueueMessage
	{
		private readonly ILogger _logger;
		private readonly IErrorHandler _errorHandler;
		protected readonly CloudQueue Queue;

		public AzureQueueManager(ILogger logger, IErrorHandler errorHandler)
		{
			_logger = logger;
			_errorHandler = errorHandler;
			var client = AzureStorage.StorageAccount.CreateCloudQueueClient();
			var queueName = GetType().ToQueueName();
			Queue = client.GetQueueReference(queueName);
		}

		#region IQueueManger Implementation

		public void AddMessage(TQueueMessage message, TimeSpan? timeToLive = null, TimeSpan? initialVisibilityDelay = null, QueueRequestOptions options = null, OperationContext operationContext = null)
		{
			Queue.AddMessage(QueueUtils.CloudQueueMessage(message), timeToLive, initialVisibilityDelay, options, operationContext);
		}

		public async Task AddMessageAsync(TQueueMessage message, TimeSpan? timeToLive = null, TimeSpan? initialVisibilityDelay = null,
			QueueRequestOptions options = null, OperationContext operationContext = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			await Queue.AddMessageAsync(QueueUtils.CloudQueueMessage(message), timeToLive, initialVisibilityDelay, options, operationContext, cancellationToken);
		}

		public void DeleteMessage(TQueueMessage message)
		{
			_logger.Info("Deleting message from queue");
			var cloudMessage = message.CloudQueueMessage;
			if (cloudMessage == null)
				return;
			_logger.Info("CloudMessage not null. Deleting");
			Queue.DeleteMessage(cloudMessage);
			_logger.Info("CloudQueue message deleted");
		}

		public async Task DeleteMessageAsync(TQueueMessage message, CancellationToken cancellationToken = default(CancellationToken))
		{
			_logger.Info("Deleting message from queue");
			var cloudMessage = message.CloudQueueMessage;
			if (cloudMessage == null)
				return;
			_logger.Info("CloudMessage not null. Deleting");
			await Queue.DeleteMessageAsync(cloudMessage, cancellationToken);
			_logger.Info("CloudQueue message deleted");
		}

		public TQueueMessage GetMessage(TimeSpan? visibilityTimeout = null, QueueRequestOptions options = null, OperationContext operationContext = null)
		{
			var cloudMessage = Queue.GetMessage(visibilityTimeout, options, operationContext);
			if (cloudMessage == null) return null;
			var message = cloudMessage.Deserialize<TQueueMessage>(_logger, _errorHandler);
			message.CloudQueueMessage = cloudMessage;
			return message;
		}

		public async Task<TQueueMessage> GetMessageAsync(TimeSpan? visibilityTimeout = null, QueueRequestOptions options = null,
			OperationContext operationContext = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			var cloudMessage = await Queue.GetMessageAsync(visibilityTimeout, options, operationContext, cancellationToken);
			if (cloudMessage == null) return null;
			var message = cloudMessage.Deserialize<TQueueMessage>(_logger, _errorHandler);
			message.CloudQueueMessage = cloudMessage;
			return message;
		}

		public TQueueMessage[] GetMultipleMessages(int maxMessages, TimeSpan? visibilityTimeout = null, QueueRequestOptions options = null, OperationContext operationContext = null)
		{
			var cloudMessages = Queue.GetMessages(maxMessages, visibilityTimeout, options, operationContext);
			if (cloudMessages == null) return null;
			var messages = new List<TQueueMessage>();
			foreach (var cloudMessage in cloudMessages)
			{
				var message = cloudMessage.Deserialize<TQueueMessage>(_logger, _errorHandler);
				message.CloudQueueMessage = cloudMessage;
				messages.Add(message);
			}

			return messages.ToArray();
		}

		public async Task<TQueueMessage[]> GetMultipleMessagesAsync(int maxMessages, TimeSpan? visibilityTimeout = null, QueueRequestOptions options = null, OperationContext operationContext = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			var cloudMessages = await Queue.GetMessagesAsync(maxMessages, visibilityTimeout, options, operationContext, cancellationToken);
			if (cloudMessages == null) return null;
			var messages = new List<TQueueMessage>();
			foreach (var cloudMessage in cloudMessages)
			{
				var message = cloudMessage.Deserialize<TQueueMessage>(_logger, _errorHandler);
				message.CloudQueueMessage = cloudMessage;
				messages.Add(message);
			}
			return messages.ToArray();
		}

		public void UpdateMessage(TQueueMessage message, TimeSpan timespan, MessageUpdateFields updateFields)
		{
			Queue.UpdateMessage(message.CloudQueueMessage, timespan, MessageUpdateFields.Visibility);
		}

		public async Task UpdateMessageAsync(TQueueMessage message, TimeSpan timespan, MessageUpdateFields updateFields, CancellationToken cancellationToken = default(CancellationToken))
		{
			await Queue.UpdateMessageAsync(message.CloudQueueMessage, timespan, updateFields, cancellationToken);
		}

		public void Clear()
		{
			Queue.Clear();
		}

		public async Task ClearAsync(CancellationToken cancellationToken = default(CancellationToken))
		{
			await Queue.ClearAsync(cancellationToken);
		}

		#endregion IQueueManger Implementation
	}
}